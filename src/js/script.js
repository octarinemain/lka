'use strict';

$(document).ready(function() {
	
	// PopUp
	function openPopUp() {
		$('.js-popup-button').on('click', function(e) {
			e.preventDefault();
			$('.popup').removeClass('js-popup-show');
			$('#helper').removeClass('blur');
			let popupClass = '.' + $(this).attr('data-popupShow');
			$(popupClass).addClass('js-popup-show');
			$('#helper').addClass('blur');
			if ($(document).height() > $(window).height()) {
				let scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
				$('html').addClass('noscroll').css('top', -scrollTop);
			}
		});
		closePopup();
	}

	// Close PopUp
	function closePopup() {
		$('.js-close-popup').on('click', function(e) {
			e.preventDefault();
			$('.popup').removeClass('js-popup-show');
			$('#helper').removeClass('blur');
			let scrollTop = parseInt($('html').css('top'));
			$('html').removeClass('noscroll');
			$('html, body').scrollTop(-scrollTop);
		});
	}
	openPopUp();
	
	// Open mobMenu
	function openMobMenu() {
		let header = $('#header');
		let wrap = header.find('.header__nav');
		let btnOpen = header.find('.mobile-menu');
		let btnClose = wrap.find('.close-menu');

		btnOpen.on('click', function(e) {
			e.preventDefault();
			wrap.addClass('active');
			$('body').addClass('open-menu');
		});

		btnClose.on('click', function(e) {
			e.preventDefault();
			wrap.removeClass('active');
			$('body').removeClass('open-menu');
		});

		$('body').on('click touchend', function(e) {
			let hasClass = $(this).hasClass('open-menu');
			let div = header.find('*');		

			if (!div.is(e.target) && div.has(e.target).length === 0 && hasClass) {				
				wrap.removeClass('active');
				$('body').removeClass('open-menu');
			}
		});
	}
	openMobMenu();

	// Jquery Validate
	$('.form').each(function () {
		$(this).validate({
			ignore: [],
			errorClass: 'error',
			validClass: 'success',
			rules: {
				name: {
					required: true,
					letters: true 
				},
				phone: {
					required: true,
					phone: true 
				},
				email: {
					required: true,
					email: true 
				},
				card: {
					required: true,
					digits: true,
					minlength: 4,
				},
				password: {
					required: true,
					normalizer: function normalizer(value) {
						return $.trim(value);
					} 
				},
				passwordRepeat: {
					required: true,
					normalizer: function normalizer(value) {
						return $.trim(value);
					} 
				},
				code: {
					required: true,
					normalizer: function normalizer(value) {
						return $.trim(value);
					} 
				},
				question: {
					required: true,
					normalizer: function normalizer(value) {
						return $.trim(value);
					} 
				}
			},
			messages: {
				phone: 'Некорректный номер',
				rules: {
					required: '' 
				},
				personalAgreement: {
					required: '' 
				},
				question: {
					required: '' 
				},
				// checkShop: {
				// 	required: '' 
				// }
			} 
		});

		jQuery.validator.addMethod('phone', function (value, element) {
			return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
		});

		jQuery.validator.addMethod('letters', function (value, element) {
			return this.optional(element) || /^([a-zа-яё]+)$/i.test(value);
		});

		jQuery.validator.addMethod('email', function (value, element) {
			return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
		});
	});

	// Masked
	$('input[type="tel"]').mask('+7(999)999-99-99', {
		autoclear: false
	});

	// $('.card').mask('9999 9999 9999 9999', {
	// 	autoclear: false
	// });

	// Checking card
	function checkCard() {
		let checkbox = $('.check-shop');

		checkbox.on('change', function() {
			let $this = $(this);
			
			if ($this.prop('checked')) {
				$this.parent()
					.next()
					.slideDown('fast');
			} else {
				$this.parent()
					.next()
					.slideUp('fast');
			}
		});
	}
	checkCard();

	// Scroll
	$('#winner-table').scrollbar();
	$('#profile-table').scrollbar();
	$('#products').find('.products-wrap__inner').scrollbar();

	let wrapScroll = $('#faq');
	let scrollInit = false;
	function initScrollMob() {
		let screenWidth = window.innerWidth;

		if (screenWidth <= 992 && !scrollInit) {
			wrapScroll.scrollbar('init');
			scrollInit = true;
			
		} else if (screenWidth > 992 && scrollInit) {
			wrapScroll.scrollbar('destroy');
			scrollInit = false;
		}
	}
	initScrollMob();

	// Select
	$('#select').select2({
		placeholder: 'Выберите вопрос',
		minimumResultsForSearch: -1
	});

	// Open FAQ
	function openFaq() {
		let wrap = $('#faq');
		let faqTitle = wrap.find('.section__faq-header');

		faqTitle.on('click', function() {
			let $this = $(this);
			let content = $this.next();

			if (content.is(':visible')) {
				$this.removeClass('active');
				content.slideUp('fast');
			} else {
				$this.addClass('active');
				content.slideDown('fast');
			}
		});
	}
	openFaq();

	// Tabs
	function innitTabs() {
		let wrap = $('#products');
		let tabsItems = wrap.find('.products-name__item');
		let tabsContent = wrap.find('.products-wrap__list');

		tabsItems.on('click', function() {
			let i = $(this).index();

			tabsItems.removeClass('active');
			$(this).addClass('active');
			tabsContent.removeClass('active');
			tabsContent.eq(i).addClass('active');
			return false;
		});
	} 
	innitTabs();

	// Resize function
	let doit; 

	function resized() {
		initScrollMob();
	}
	window.onresize = function() { 
		clearTimeout(doit); 
		doit = setTimeout(function() { 
			resized(); 
		}, 100); 
	};

});